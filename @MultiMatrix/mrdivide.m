function C = mrdivide(A,B)
%	Compute matrix right division of each slice in the 3rd dimmension of A
%   and by the corresponding slice of B.
%
%		C(:,:,k) = A(:,:,k) / B(:,:,k) for every k
%
%	To speed up the computation, the result is obtained by the performing
%	the division of a block-row matrix and a block-diagonal matrix.
%
%                                                      ________________________
%                                                     |    |    |    |    |    |
%    ____________________     ____________________    | B1 |    |    |    |    |
%   |                    |   |                    |   |    | B2 |    |    |    |
%   | C1, C2, C3, ... CK | = | A1, A2, A3, ... AK | / |    |    | B3 |    |    |
%   |____________________|   |____________________|   |    |    |    | .. |    |
%                                                     |    |    |    |    | BK |
%                                                     |____|____|____|____|____|
%
%	where Ak = A(:,:,k)
%
%	NOTE 1: The block diagonal matris is a sparse matrix created directly by
%	specifiying the indexes of the diagonal blocks. (I havent found a faster
%	way to do this, maybe it does not exist).
%
%	NOTE 2: This function is meant to compute matrix-division of two vectors
%	of matrices with the proper dimensions: size(A)=[N M K], size(B)=[M L K]
%	It do not compute scalar-matrix product or product of two matrix-vectors
%	with different number of slices.
%
%   NOTE 3: When A is a MultiMatrix, and B a single matrix, we use a
%   simplified version where the sparse block diagonal matrix full of the
%   same matrix B1 is avoided
%          ____     ____
%         |    |   |    |
%         | C1 |   | A1 |
%         |    |   |    |    ____
%         | C2 |   | A2 |   |    |
%         |    | = |    | / | B1 |
%         | .. |   | .. |   |____|
%         |    |   |    |   
%         | CK |   | AK |
%         |____|   |____|   

if isscalar(A)||isscalar(B)
    C = MultiMatrix(builtin('rdivide',A,B));
else
    % check sizes
    [m1, n1, f1] = size(A);
    [m2, n2, f2] = size(B);
        
    % check if arguments need to be expanded
    if f1 == 1
        A = repmat(A, [1 1 f2]);
        f1 = f2;
    elseif f2 == 1
        % stack all matrices of A into a column
        At = reshape(permute(A, [1 3 2]), [m1*f1 n1]);
        % perform the mrdivide as in NOTE 3
        C = builtin('mrdivide',At,B);
        % All matrices of C are now stacked in a column, make it into a MultiMatrix
        C = MultiMatrix(ipermute(reshape( C , [m1 f1 m2] ), [1 3 2]));
        return
    end
    
    % compute block-row matrix
    A = reshape(A,m1,n1*f1);
    
    % compute block-diagonal sparse matrix
    B = MultiMatrix.MM2Sparse(B);
    
    % perform actual division
    C = MultiMatrix(builtin('mrdivide',A,B));
    
    % format output with the standar dimensions
    C = reshape(C,m1,m2,f1);
    
end
end
