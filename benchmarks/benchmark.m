clear variables
clc
close all

% F is the amount of matrices in the problem
F = 1e4;

% tries is the amount of repetitions for each experiment
tries = 10;

% OPERATOR is the operator to test. It can be @mtimes, @mldivide @mrdivide
OPERATOR = @mtimes;

% t will be used internally
t=zeros(tries,1);
for N=2:20
    % generate some matrices with random data
    A = rand(N,N,F);
    B = rand(N,N,F);
    % preallocate the space for the result
    C = zeros(N,N,F);
    
    % for-loop
    for tt=1:tries
        tic
        for ff=1:F
            C(:,:,ff) = OPERATOR(A(:,:,ff),B(:,:,ff));
        end
        t(tt)=toc;
    end
    disp([num2str(N) ' for-loop time: ' num2str(mean(t))]);
    fortime(N)=mean(t);
    
    % MultiMatrix product
    t=zeros(tries,1);
    Amulti = MultiMatrix(A);
    Bmulti = MultiMatrix(B);
    for tt=1:tries
        tic
        C = OPERATOR(Amulti,Bmulti);
        t(tt)=toc;
    end
    disp([num2str(N) ' MultiMatrix: ' num2str(mean(t))]);
    sparseTime(N)=mean(t);
    
    % cellfun
    Acell=num2cell(double(A),[1,2]);
    Bcell=num2cell(double(B),[1,2]);
    for tt=1:tries
        tic
        C = cellfun(OPERATOR,Acell,Bcell,'UniformOutput',false);
        t(tt)=toc;
    end
    disp([num2str(N) ' cellfun: ' num2str(mean(t))]);
    cellfunTime(N)=mean(t);
    
    disp(' ')
end

%% plot the result
figure(8365345)
clf
hold on
plot(fortime,'-+');
plot(sparseTime,'-+');
plot(cellfunTime,'-+');
legend('for','sparseProd','cellfunTime')
title(sprintf('test for %s for twp NxNx%d matrices',char(OPERATOR),F));
xlabel('Matrix size N')
ylabel('time')



