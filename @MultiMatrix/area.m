function hand=area(Obj,varargin)
% TOUCHSTONE/PLOT  Plot scattering parameters.
%
%   C=PLOT(A(i,i),Unit) plot the element (i,i) of the object A in the units
%   specified by the argument Unit and returns a cell array with the handle
%   to each plotted line. Unit may be one of the following strings:
%
%       m : Modulus
%       s : Square modulus
%       a : Phase in radians
%       w : Phase wrap to [-2Pi 0]
%       d : Decibels
%       D : Phase in degrees
%       R : Real part
%       I : Imaginary part
%       N : Nyquist plot
%
%   C=PLOT(A(i,i),Unit,'Xaxis',F) plot the element (i,i) of the object A in
%   and with the X axis specified by vector F.
%
%   C=PLOT(A,Unit,'Xaxis',F) plot all vectors in A (A(1,1), A(1,2), A(2,1) ...)
%   of the object A in and with the X axis specified by vector F.
%
%   C=PLOT(A,Unit,'Prop1',value,'Prop2',value) plot the data in A with the
%   correspondent units and pass all Name-value pairs of properties to the
%   plot function.
%
%   NOTE 1: Unit can be a 2D-char-array to obtain multiple subplots
%
%   'RI' :  Provides two plots arranged in column with real and imaginary parts
%   'dm' :  Provides two plots arranged in column with the modulus in
%           db and the modulus in linear scale.
%   ['dm';'ar']: Provides a 2x2 plots matrix with modulus in
%           db and  linear in the first row, and phase in degrees and
%           radians in the second row.
%
%   Example:
%
%   C=PLOT(A(1,1),'RI','Color','r','LineStyle','--','DisplayName','myData')
%   creates two subplots side by side plotting the real and the imaginary
%   parts of the data in A with a dashed red line and set the name 'myData'
%   as a legend entry.
%
%
%   NOTE 2: For convenience, it is better to specify the desired order for
%   the 'Color' property and the 'LineStyle' property in order not to
%   specify it every time. This order is defined by the ColorOrder and
%   LineStyleOrder properties of the axes. For instance, to set the default
%   LineStyleOrder do:
%
%   >> set(groot,'defaultAxesLineStyleOrder',{'-*',':','o'})
%
%   and to set the desired color order (already nice by default) do:
%
%   >> set(groot,'defaultColorOrder',{'r','b','y', ...})
%
%
%   See also:
%
%   Author: David Martinez <mtnez.david@gmail.com>


%% Parse input parameters
validConfig = {'d','m','s','a','w','D','R','I','N'};
validateConfig = @(x) all(ismember(cellstr(x(:)),validConfig));
p=inputParser;
p.addRequired('Obj',@(x) isa(x,'MultiMatrix'));
p.addParameter('Config','d',validateConfig);
p.addParameter('Xaxis',1:length(Obj),@isvector);
p.addParameter('Xlabel',[],@ischar);
p.KeepUnmatched = 1;
p.parse(Obj,varargin{:});
args=p.Results;

%% Plot options
options = [fieldnames(p.Unmatched).';struct2cell(p.Unmatched).'];
%% Number of subplots
[q,r] = size(args.Config);

%% Number of parameters
[j,i,~] = size(args.Obj);

%% Permute data to avoid reshape
args.Obj = permute(args.Obj,[3 2 1]);

%% Xaxis for each parameter
args.Xaxis = repmat(args.Xaxis(:), [1 i j]);

%% Prealocate output argument
hand=cell(q,r);

%% compute function to plot in the chosen units
for k=1:q
    for l=1:r  
        switch args.Config(k,l)
            case 'd'
                f=20*log10(abs(args.Obj));
                y='Magnitude (dB)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'm'
                f=abs(args.Obj);
                y='Magnitude (lin)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 's'
                f=abs(args.Obj.Data).^2;
                y='Square modulus';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'D'
                f=angle(args.Obj).*180./pi;
                y='Phase (deg)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'w'
                f=wrapTo2Pi(angle(args.Obj))-2*pi;
                y='Phase (rad)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'a'
                f=angle(args.Obj);
                y='Phase (rad)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'R'
                f=real(args.Obj);
                y='Real';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'I'
                f=imag(args.Obj);
                y='Imaginary';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'N'
                f=imag(args.Obj);
                Xaxis=real(args.Obj);
                x='Real';
                y='Imag';
        end
        
        f = num2cell(f,1);
        Xaxis = num2cell(Xaxis,1);
        
        % Add frequency axis to each vector
        plotData=[Xaxis(:), f(:)].';
        
        %% Actual Plot
        subplot(q,r,(k-1)*r+l);
        grid on; hold on;
        hand{k,l}=area(plotData{:}, options{:});
        ylabel(y);
        xlabel(x);
    end
end
