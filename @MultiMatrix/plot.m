function hand=plot(Obj,varargin)
% TOUCHSTONE/PLOT  Plot scattering parameters.
%
%   C=PLOT(A(i,i),Unit) plot the element (i,i) of the object A in the units
%   specified by the argument Unit and returns a cell array with the handle
%   to each plotted line. Unit may be one of the following strings:
%
%       m : Modulus
%       s : Square modulus
%       a : Phase in radians
%       w : Phase wrap to [-2Pi 0]
%       d : Decibels
%       D : Phase in degrees
%       R : Real part
%       I : Imaginary part
%       N : Nyquist plot
%
%   C=PLOT(A(i,i),Unit,'Xaxis',F) plot the element (i,i) of the object A in
%   and with the X axis specified by vector F.
%
%   C=PLOT(A,Unit,'Xaxis',F) plot all vectors in A (A(1,1), A(1,2), A(2,1) ...)
%   of the object A in and with the X axis specified by vector F.
%
%   C=PLOT(A,Unit,'Prop1',value,'Prop2',value) plot the data in A with the
%   correspondent units and pass all Name-value pairs of properties to the
%   plot function.
%
%   NOTE 1: Unit can be a 2D-char-array to obtain multiple subplots
%
%   'RI' :  Provides two plots arranged in column with real and imaginary parts
%   'dm' :  Provides two plots arranged in column with the modulus in
%           db and the modulus in linear scale.
%   ['dm';'ar']: Provides a 2x2 plots matrix with modulus in
%           db and  linear in the first row, and phase in degrees and
%           radians in the second row.
%
%   Example:
%
%   C=PLOT(A(1,1),'RI','Color','r','LineStyle','--','DisplayName','myData')
%   creates two subplots side by side plotting the real and the imaginary
%   parts of the data in A with a dashed red line and set the name 'myData'
%   as a legend entry.
%
%
%   NOTE 2: For convenience, it is better to specify the desired order for
%   the 'Color' property and the 'LineStyle' property in order not to
%   specify it every time. This order is defined by the ColorOrder and
%   LineStyleOrder properties of the axes. For instance, to set the default
%   LineStyleOrder do:
%
%   >> set(groot,'defaultAxesLineStyleOrder',{'-*',':','o'})
%
%   and to set the desired color order (already nice by default) do:
%
%   >> set(groot,'defaultColorOrder',{'r','b','y', ...})
%
%
%   See also:
%
%   Author: David Martinez <mtnez.david@gmail.com>


%% Parse input parameters
validConfig = {'d','m','s','a','w','W','D','R','I','N'};
validateConfig = @(x) all(ismember(cellstr(x(:)),validConfig));
p=inputParser;
p.addRequired('Obj',@(x) isa(x,'MultiMatrix'));
p.addParameter('Config','d',validateConfig);
p.addParameter('Xaxis',1:length(Obj),@(x) isvector(squeeze(x)));
p.addParameter('Xlabel',[],@ischar);
p.addParameter('DisplayName', [] , @ischar);
p.addParameter('logx',false,@islogical);
p.addParameter('logy',false,@islogical);
p.KeepUnmatched = 1;
p.parse(Obj,varargin{:});
args=p.Results;

%% Number of subplots
[q,r] = size(args.Config);

%% Number of parameters
[j,i,~] = size(args.Obj);

%% Repmat legend for each line and add  parameter number
if j~=1 || i ~= 1
    [J,I] = meshgrid(1:i,1:j);
    args.DisplayName = [repmat(args.DisplayName,j*i,1), num2str(J(:)),num2str(I(:))];
end
            
%% Permute data to avoid reshape
args.Obj = permute(double(args.Obj),[3 2 1]);

%% Xaxis for each parameter
args.Xaxis = repmat(args.Xaxis(:), [1 i j]);

%% Prealocate output argument
hand=cell(q,r);

%% compute function to plot in the chosen units
for k=1:q
    for l=1:r  
        switch args.Config(k,l)
            case 'd'
                f=20*log10(abs(args.Obj));
                y='Magnitude (dB)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'm'
                f=abs(args.Obj);
                y='Magnitude (lin)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 's'
                f=abs(args.Obj).^2;
                y='Square modulus';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'D'
                f=angle(args.Obj).*180./pi;
                y='Phase (deg)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'w'
                f=wrapTo2Pi(angle(args.Obj))-2*pi;
                y='Phase (rad)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'W'
                f=wrapTo(angle(args.Obj),pi);
                y='Phase (rad)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'a'
                [F,M,N]=size(args.Obj);
                f = zeros(F,M,N);
                for mm=1:M
                    for nn=1:N
                        f(:,mm,nn)=unwrap(squeeze(angle(args.Obj(:,mm,nn))));
                    end
                end
                y='Phase (rad)';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'R'
                f=real(args.Obj);
                y='Real';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'I'
                f=imag(args.Obj);
                y='Imaginary';
                x=args.Xlabel;
                Xaxis=args.Xaxis;
            case 'N'
                f=imag(args.Obj);
                Xaxis=real(args.Obj);
                x='Real';
                y='Imag';
        end
        
        f = num2cell(f,1);
        Xaxis = num2cell(Xaxis,1);
        
        % Add frequency axis to each vector
        plotData=[Xaxis(:), f(:)].';
        
        %% Actual Plot
        if (q~=1)||(r~=1)
            subplot(q,r,(k-1)*r+l);
        end
        if args.logx
            if args.logy
                % logx and logy
                hand{k,l}=loglog(plotData{:}, p.Unmatched);
            else
                % logx, no logy
                hand{k,l}=semilogx(plotData{:}, p.Unmatched);
            end
        else
            if args.logy
                % nologx, logy
                hand{k,l}=semilogy(plotData{:}, p.Unmatched);
            else
                % no logx, no logy
                hand{k,l}=plot(plotData{:}, p.Unmatched);
            end
        end
        if ~isempty(args.DisplayName)
            set(hand{k,l},{'DisplayName'},cellstr(args.DisplayName));
        end
        ylabel(hand{k,l}(1,1).Parent,y);
        xlabel(hand{k,l}(1,1).Parent,x);
        grid(hand{k,l}(1,1).Parent,'on');
        hold(hand{k,l}(1,1).Parent,'on');
    end
end
