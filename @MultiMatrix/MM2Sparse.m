function [B] = MM2Sparse(A)
% MM2Sparse gathers the different matrices in a MultiMatrix and puts them
% in a block diagonal sparse matrix

% check size
[m,n,k] = size(A);

% Compute indexes
J = repmat(1:n*k,[m,1]);
I = repmat(reshape(1:m*k,m,[]),[n,1]);

% Call sparse (colon operator is slower)
B = sparse(reshape(I,[],1),reshape(J,[],1),reshape(A,[],1));
