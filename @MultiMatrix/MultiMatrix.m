classdef MultiMatrix < double
    %MULTIMATRIX
    methods
        %% Constructor
        function obj = MultiMatrix(Data)
            obj@double(Data);
            % call the checkData function
            obj.checkData(Data);
        end
        
        %% Define some of the dimensional functions of matlab
        function res = length(obj)
            res = size(obj,3);
        end
        
        %% Make sure that arithmetic operator return a MultiMatrix
        function obj1 = plus(obj1,obj2)
            obj1 = MultiMatrix(builtin('plus',obj1,obj2));
        end
        function obj1 = sum(varargin)
            obj1 = MultiMatrix(builtin('sum',varargin{:}));
        end
        function obj1 = minus(obj1,obj2)
            obj1 = MultiMatrix(builtin('minus',obj1,obj2));
        end
        function obj1 = times(obj1,obj2)
            obj1 = MultiMatrix(builtin('times',obj1,obj2));
        end
        function obj1 = prod(varargin)
            obj1 = MultiMatrix(builtin('prod',varargin{:}));
        end
        function obj1 = rdivide(obj1,obj2)
            obj1 = MultiMatrix(builtin('rdivide',obj1,obj2));
        end
        function obj = transpose(obj)
            obj = MultiMatrix(permute(obj,[2 1 3]));
        end
        function obj = ctranspose(obj)
            obj = MultiMatrix(conj(permute(obj,[2 1 3])));
        end
        %% Trace function
        function d = diag(obj)
            [a, b, ~] = size(obj);
            n = min(a,b);
            m = max(a,b);
            if n ==1
                d = obj .* eye(m);
            else
                d = sum( obj(1:n,1:n,:) .* eye(n), 2,'native');
            end
        end
        
        function tr = trace(obj)
            tr = sum(diag(obj),1);
        end
        function p = innerprod(obj1,obj2)
            p = sum(sum(obj1.*obj2,1),2);
        end
    end
    methods (Static)
        function tf = checkData(data)
            if ndims(data)>3
                error('MultiMatrix:WrongDimensions','The data should be maximum a NxMxF matrix')
            end
            tf = true;
        end
        B = MM2Sparse(obj)
        B = blkdiag(varargin)
        B = run(fun,varargin)
    end
end
