function C = mtimes(A,B)
%	Compute the matrix product of each slice in the 3rd dimmension of A and
%   by the corresponding slice of B.
%
%		C(:,:,k) = A(:,:,k) * B(:,:,k) for every k
%
%	To speed up the computation, the result is obtained by the performing
%	the product of a block-diagonal matrix and a block-column matrix
%
%                                                      ________________________
%                                                     |    |    |    |    |    |
%    ____________________     ____________________    | B1 |    |    |    |    |
%   |                    |   |                    |   |    | B2 |    |    |    |
%   | C1, C2, C3, ... CK | = | A1, A2, A3, ... AK | * |    |    | B3 |    |    |
%   |____________________|   |____________________|   |    |    |    | .. |    |
%                                                     |    |    |    |    | BK |
%                                                     |____|____|____|____|____|
%
%	where Ak = A(:,:,k)
%
%	NOTE 1: The block diagonal matrix is a sparse matrix created using the blkdiag3d function
%
%	NOTE 2: This function is meant to compute matrix-multiplication of two vectors
%	of matrices with the proper dimensions: size(A)=[N M K], size(B)=[M L K]
%	It do not compute scalar-matrix product or product of two matrix-vectors
%	with different number of slices.
%
%
%   NOTE 3: When A is a single matrix and B is a MultiMatrix, there is no
%   need to create a block diagonal matrix, the operation is then simplified to:
%    ____________________     ____     ________________________
%   |                    |   |    |   |                        |
%   | C1, C2, C3, ... CK | = | A1 | * | B1 | B2 | B3 | .. | BK |
%   |____________________|   |____|   |________________________|
%
%
%   NOTE 4: When B is a single matrix and A is a MultiMatrix, there is no
%   need to create a block diagonal matrix, then we perform the following:
%          ____     ____
%         |    |   |    |
%         | C1 |   | A1 |
%         |    |   |    |    ____
%         | C2 |   | A2 |   |    |
%         |    | = |    | * | B1 |
%         | .. |   | .. |   |____|
%         |    |   |    |
%         | CK |   | AK |
%         |____|   |____|


% check whether one of the arguments is a scalar. If this is the case, call the dot product
if isscalar(A)||isscalar(B)
    C = MultiMatrix(builtin('times',A,B));
else
    C = MultiMatrix(MultiMatrixMtimes(double(A),double(B)));
end
end

function C = MultiMatrixMtimes(A,B)
    [m1, n1, f1] = size(A);
    [m2, n2, f2] = size(B);
    % check if one of the arguments is a single matrix
    % if this is the case, we can use a faster version
    if f1 == 1
        % left multiply of multimatrix with single matrix (see NOTE 3)
        C = reshape(A*B(:,:),[m1 n2 f2]);
        return
    elseif f2 == 1
        % right multiply of multimatrix with single matrix (see NOTE 4)
        A = reshape(permute(A, [1 3 2]), [(m1*n1*f1)/n1 n1]);
        C = permute(reshape(A*B, [m1 f1 n2]), [1 3 2]);
        return
    end
A = reshape(A,m1,n1,1,f1);
B = reshape(B,1,m2,n2,f2);
C = sum(A .* B,2);
C = reshape(C,m1,n2,f2);
end



