# MultiMatrix

Matlab code which allows easy and fast handling of sets of same-sized matrices.

Possible usefull usage cases include:

+ Working with Multiple-Input multiple-output frequency response functions
+ ...

## Examples

### Overloaded matrix operators

Without MultiMatrices, the product of two sets of matrices would require a for-loop, or could be done using cellfun or bsxfun:


```matlab
F = 1e5;
A = rand(2,2,F);
B = rand(2,2,F);
C = zeros(2,2,F);
for ff=1:F
  C(:,:,ff) = A(:,:,ff)*B(:,:,ff);
end
```

When using MultiMatrices, this code is simplified into


```matlab
F = 1e5;
A = MultiMatrix(rand(2,2,F));
B = MultiMatrix(rand(2,2,F));
C = A*B;
```

Other operators which are supported include: * / \\ + -

### Plotting

The plotting of these sets of matrices in Matlab can be a hassle:

```matlab
A = rand(2,2,1e5);
figure;hold on
for mm=1:size(A,1)
  for nn=1:size(A,2)
    plot(squeeze(A(mm,nn,:)));
  end
end
```

With MultiMatrices, the plot function does all this automatically

```matlab
A = MultiMatrix(rand(2,2,1e5));
figure
plot(A);
```

The `area` and `polar` plot functions are similarly available

## Requirements

Requires at least Matlab 2016b. Otherwise, the .*, + and - operators will not perform expansion automatically
