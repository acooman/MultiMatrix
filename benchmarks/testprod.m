% test product
clear variables
% close all;
clc;

N = 1:15;
F = round(logspace(0,4,20));

% functions_to_test = {@nopermuteprod,@permuteprod,@forloopprod,@freqforloopprod,@mmxprod,@mmxprod_naive};
functions_to_test = {@nopermuteprod,@permuteprod,@mmxprod};

% profile on
complex = true;

% pre-allocate the time and median cells
medians = cell(size(functions_to_test));
[medians{:}] = deal(zeros(length(F),length(N)));

% run the actual tests
for f=1:length(F)
    for k = 1:length(N)
        
        disp(['Startin iteration ' num2str(f) ',' num2str(k) ' of ' num2str(length(F)) ',' num2str(length(N))])
        % generate the data
        if complex
            A = rand(N(k),N(k),F(f))+1i*rand(N(k),N(k),F(f));
            B = rand(N(k),N(k),F(f))+1i*rand(N(k),N(k),F(f));
        else
            A = rand(N(k),N(k),F(f));
            B = rand(N(k),N(k),F(f));
        end
        % run all the functions
        for p=1:length(functions_to_test)
            fprintf('   %s: ',func2str(functions_to_test{p}))
            medians{p}(f,k) = timeit(@()functions_to_test{p}(A,B));
            fprintf('%e\n',medians{p}(f,k));
        end
    end
end

% save results_adam_fixed F N mean*

%%
fun = @log10;
alpha = 1;
leg={};
figure(123456)
clf
grid on; hold on;
cmap = colormap('lines');
for p=1:length(functions_to_test)
    surf(N,log10(F),fun(medians{p}),'FaceColor',cmap(p,:),'EdgeColor',cmap(p,:),'FaceAlpha',alpha,'DisplayName',func2str(functions_to_test{p}));
end
legend('show','Location','best');
xlabel('matrix size')
ylabel('log_{10}(frequencies)')
zlabel([func2str(fun) '(time)'])
view(0,-90)
%% Functions
function C = permuteprod(A,B)

[m1,n1,~] = size(A);
[m2,n2,l] = size(B);
if le(m1,n2)
    % stack all matrices of A into a column
    A = reshape(permute(A, [2 3 1]), n1*l,1, m1);
    % stack all matrices of B into a column in the third dimension
    B = reshape(permute(B, [1 3 2]), m2*l,n2);
    % Multiply
    C = A .* B;
    % sum
    C = reshape(C,n1,n2*l,m1);
    C = sum(C,1);
    % Set dimensions of output
    C = permute(reshape(C,l,n2,m1),[3,2,1]);
else
    % stack all matrices of A into a column
    A = reshape(permute(A, [2 3 1]), n1*l, m1);
    % stack all matrices of B into a column in the third dimension
    B = reshape(permute(B, [1 3 2]), m2*l,1,n2);
    % Multiply
    C = A .* B;
    % sum
    C = reshape(C,m2,m1*l,n2);
    C = sum(C,1);
    % Set dimensions of output
    C = permute(reshape(C,l,m1,n2),[2,3,1]);
end

end

function C = forloopprod(A,B)
[m,n,l] = size(B);
[r,t,s] = size(A);
C = zeros(r,n,l);
for i=1:n
    C = C + A(:,i,:) .*B(i,:,:);
end
end

function C = freqforloopprod(A,B)
[m,n,f] = size(B);
[r,t,~] = size(A);
C = zeros(r,n,f);
for ff=1:f
    C(:,:,ff) = A(:,:,ff) * B(:,:,ff);
end
end

function C = freqcellfunprod(A,B)
Acell=num2cell(A,[1,2]);
Bcell=num2cell(B,[1,2]);
C = cellfun(@mtimes,Acell,Bcell,'UniformOutput',false);
C = cat(3,C{:});
end

function C = nopermuteprod(A,B)
[m1,n1,f1] = size(A);
[m2,n2,f2] = size(B);

A = reshape(A,m1,n1,1,f1);
B = reshape(B,1,m2,n2,f2);
C = sum(A .* B,2);
C = reshape(C,m1,n2,f2);
end


function C = sparseprod(A,B)
% check size
[m1,n1,f1] = size(A);
[m2,n2,f2] = size(B);
% compute block-row matrix
A = reshape(A,m1,n1*f1);
% Compute indexes
J = repmat(1:n2*f2,[m2,1]);
I = repmat(reshape(1:m2*f2,m2,[]),[n2,1]);
% Call sparse (colon operator is slower)
B = sparse(reshape(I,[],1),reshape(J,[],1),reshape(A,[],1));
% perform actual multiplication, format output with the standar dimensions
C = reshape(A*B,m1,n2,f1);
end

function C = mmxprod(A,B)
    [rA,cA,fA]=size(A);
    [rB,cB,fB]=size(B);
    if rA*cA*cB*max(fA,fB)>100000
    C = mmx_complex(A,B);
    else
        C = mmx_nothreads(A,B);
    end
end

