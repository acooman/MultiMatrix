function K = kron(A,B)
% kronecker product of two MultiMatrices

% cast A and B to double
A = double(A);
B = double(B);

[ma,na,fa] = size(A);
[mb,nb,fb] = size(B);

% make sure A and B have the same size
if fa==1
    A = repmat(A,1,1,fb);
end

% perform the kron
A = reshape(A,[1 ma 1 na fa]);
B = reshape(B,[mb 1 nb 1 fb]);
K = reshape(A.*B,[ma*mb na*nb fa]);

% cast the result to a MultiMatrix
K = MultiMatrix(K);

end