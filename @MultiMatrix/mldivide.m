function C = mldivide(A,B)
%	Compute matrix left division of each slice in the 3rd dimmension of A
%   and by the corresponding slice of B.
%
%		C(:,:,k) = A(:,:,k) \ B(:,:,k) for every k
%
%	To speed up the computation, the resunt is obtained by the performing
%	the division of  a block-diagonal matrix and a block-column matrix.
%
%	 ____       _____________________________        ____
%	| C1 |     | A1 |    |    |    |    |    |	    | B1 |
%	| C2 |     |    | A2 |    |    |    |    |	    | B2 |
%	| C3 |     |    |    | A3 |    |    |    |   	| B3 |
%	| C4 |  =  |    |    |    | A4 |    |    |   \	| B4 |
%	|  : |     |    |    |    |    | .. |    |	    |  : |
%	| CK |     |    |    |    |    |    | AK |	    | BK |
%	|____|     |____|____|____|____|____|____|	    |____|
%
%	where Ak = A(:,:,k)
%
%	NOTE 1: The block diagonal matrix is a sparse matrix created directly by
%	specifiying the indexes of the diagonal blocks. (I havent found a faster
%	way to do this, maybe it does not exist).
%
%	NOTE 2: This function is meant to compute matrix-division of two vectors
%	of matrices with the proper dimensions: size(A)=[N M K], size(B)=[M L K]
%	It do not compute scalar-matrix product or product of two matrix-vectors
%	with different number of slices.
%
%   NOTE 3: When A is a single matrix, and B a MultiMatrix, we use a
%   simplified version where the sparse block diagonal matrix full of the
%   same matrix A1 is avoided
%    ____________________     ____     ________________________ 
%   |                    |   |    |   |                        |
%   | C1, C2, C3, ... CK | = | A1 | \ | B1 | B2 | B3 | .. | BK |
%   |____________________|   |____|   |________________________|
%

if isscalar(A)||isscalar(B)
    C = MultiMatrix(builtin('ldivide',A,B));
else
    % check sizes
    [m1,n1, f1] = size(A);
    [~, n2, f2] = size(B);
    
    % check if arguments need to be expanded
    if f1 == 1
        % when A is only a single matrix, we use a simpler and faster version
        C = MultiMatrix(reshape(builtin('mldivide',A,B(:,:)), [n1 n2 f2]));
        return
    elseif f2 == 1
        B = repmat(B, [1 1 f1]);
        f2 = f1;
    end
    
    % compute block-diagonal sparse matrix
    A = MultiMatrix.MM2Sparse(A);
    
    % compute block-column matrix
    B = reshape(permute(B,[1 3 2]), m1*f1, []);
    
    % perform actual division
    C = MultiMatrix(builtin('mldivide',A,B));
    
    % format output with the standar dimensions
    C = permute(reshape(C, [n1 f2 n2]),[1 3 2]);
    
end
end

