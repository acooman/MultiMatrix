function res = blkdiag( varargin )
%BLKDIAG combines many MultiMatrices into a large block-diagonalmultimatrix
%
%   MM = blkdiag( A , B , ... )


% Compute sizes of each cell
[m,n,f] = cellfun(@size,varargin);
F=max(f);

% repmat the multiMatrices of length 1 to match the length of the other ones
if F~=1 && any(f==1)
    varargin(f==1) = cellfun(@(x) repmat(x,[1 1 F]),varargin(f==1),'uniformOutput',false);
    f(f==1)=F;
end

% check the length of the provided MultiMatrices
if ~all(f==F)
    error('All provided multiMatrices should have the same length');
end

% Cumpute first and last index of each block
M = [0, cumsum(m)];
N = [0, cumsum(n)];
mr = M(end);
nr = N(end);

% Construct logical blkdiag matrix (faster than integers) 
ind = false(mr,nr);
for k=1:nargin
    ind(M(k)+1:M(k+1),N(k)+1:N(k+1)) = true(m(k),n(k));
end
IND = repmat(ind,1,1,F);

% Reshape and cat data
DATA = cellfun(@(x) reshape(x,[],F),varargin,'uniformOutput',false);
DATA = vertcat(DATA{:});

% Create matrix with zeros and fill it
res = MultiMatrix(zeros(size(IND)));
res(IND) = DATA(:);


end

