function run_MultiMatrix_tests()
% go to the right folder
fi = mfilename('fullpath');
path = fileparts([fi,'.m']);
cd(path)
cd ..

import matlab.unittest.TestSuite
import matlab.unittest.TestRunner
import matlab.unittest.plugins.CodeCoveragePlugin

%% test the FRM class
codeFolders = {fullfile(pwd,'@MultiMatrix')};
suite = TestSuite.fromFile(fullfile('tests','testMultiMatrix.m'));
runner = TestRunner.withTextOutput;
runner.addPlugin(CodeCoveragePlugin.forFolder(codeFolders));
result = runner.run(suite);


end

