function res = run( fun , varargin )
%OPERATION runs a function specified in the handle on a set of multimatrices
%   
%	res = run( fun , A , B , ... )
% 
% The function converts the multimatrices into a cell array of normal
% matrices and then uses cellfun to perform the calculation in the function
% handle. 
% Due to the implementation of cellfun in matlab, the operation is
% performed on all cores of the computer.
%
% EXAMPLE: multiply two multimatrices:
%       C = operation( @mtimes , A , B )

if nargin < 2
    error('Not enough input arguments.');
end

% convert each multimatrix to a cell array of 2-d matrices
varargin = cellfun(@(x) num2cell(double(x),[1 2]),varargin,'uniformOutput',false);

% get the length of each provided MultiMatrix
f = cellfun(@(x) size(x,3),varargin);

% if one of the multimatrices needs repmatting, do that
F = max(f);
if (F~=1)&& any(f==1)
    varargin(f==1) = cellfun(@(x) repmat(x,[1 1 F]),varargin(f==1),'uniformOutput',false);
end

% fun the function on the set of multimatrices
TEMP = cellfun(fun,varargin{:},'UniformOutput',false);

% convert the result back into a MultiMatrix
res = MultiMatrix(cat(3,TEMP{:}));

end

