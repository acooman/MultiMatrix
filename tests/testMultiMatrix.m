classdef testMultiMatrix < matlab.unittest.TestCase
    %TESTFRM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (TestParameter)
        scalar_operator = struct('PLUS',@plus,'MINUS',@minus,'TIMES',@times,'RDIVIDE',@rdivide,'LDIVIDE',@ldivide);
        matrix_operator = struct('MTIMES',@mtimes,'MLDIVIDE',@mldivide,'MRDIVIDE',@mrdivide);
        F = struct('small', 1,'medium', 2, 'large', 10);
        F2 = struct('equal',Inf,'single',1);
        N = struct('one', 1,'two', 2, 'six', 6);
        M = struct('one', 1,'two', 2, 'six', 6);
        I = struct('ten', 6);
        PlotTypes = {[],'s','a','m','w','d','D','R','I','N'};
    end
    
    methods (Test)
        %% CONSTRUCTOR
        function test_constructor(testCase,N,F)
            % verify that the constructor works when a double array is provided
            IN = MultiMatrix(rand(N,N,F));
            % when a MultiMatrix is provided, it should also work
            IN = MultiMatrix(IN);
            % try to put in some weird stuff.
            % If the ADAM error is reached, the multiMatrix is not properly checked
%             try
%                 MultiMatrix('abc');
%                 MultiMatrix({1});
%                 MultiMatrix(struct());
%                 error('ADAM');
%             catch err
%                 testCase.verifyEqual(err.identifier,'MultiMatrix:InvalidData');
%             end
            try
                MultiMatrix(rand(2,2,2,2));
            catch err
                testCase.verifyEqual(err.identifier,'MultiMatrix:WrongDimensions');
            end
        end
        %% OPERATORS
        function test_matrix_operators(testCase,matrix_operator,N,M,I,F,F2)
            switch char(matrix_operator)
                case 'mrdivide'
                    IN1 = rand(N,I,F)+1i*rand(N,I,F);
                    IN2 = rand(M,I,1)+1i*rand(M,I,1);
                case 'mldivide'
                    IN1 = rand(I,N,F)+1i*rand(I,N,F);
                    IN2 = rand(I,M,1)+1i*rand(I,M,1);
                otherwise
                    IN1 = rand(N,I,F)+1i*rand(N,I,F);
                    IN2 = rand(I,M,1)+1i*rand(I,M,1);
            end
            % calculate the expected value with a simple for-loop
            for ff=1:F
                expected(:,:,ff) = matrix_operator(IN1(:,:,ff),IN2(:,:));
            end
            % if the provided F2 is infinite, repeat F2 as many times as
            % needed to match the size of F
            if isinf(F2)
                IN2 = repmat(IN2,[1 1 F]);
            end
            % use the operator defined in the class
            RES = matrix_operator(MultiMatrix(IN1),MultiMatrix(IN2));
            % compare the obtained result to the expected result
            testCase.verifyEqual(double(RES) , expected,'AbsTol',1e-10)
            % check whether we can apply the operator by double arrays on the right
            RES = matrix_operator(MultiMatrix(IN1),IN2);
            % compare the obtained result to the expected result
            testCase.verifyEqual(double(RES) , expected,'AbsTol',1e-10)
            % check whether we can apply the operator by double arrays on the left
            RES = matrix_operator(IN1,MultiMatrix(IN2));
            % compare the obtained result to the expected result
            testCase.verifyEqual(double(RES) , expected,'AbsTol',1e-10)
            % test whether the operator works with a scalar
            RES = matrix_operator(MultiMatrix(IN1),rand());
            RES = matrix_operator(rand(),MultiMatrix(IN1));
        end
        function test_scalar_operators(testCase,scalar_operator,N,M,F,F2)
            IN1 = rand(N,M,F)+1i*rand(N,M,F);
            if isinf(F2)
                IN2 = rand(N,M,F)+1i*rand(N,M,F);
                % calculate the expected value with a simple for-loop
                expected = scalar_operator(IN1,IN2);
            else
                IN2 = rand(N,M)+1i*rand(N,M);
                % calculate the expected value with a simple for-loop
                expected = scalar_operator(IN1,repmat(IN2,[1 1 F]));
            end
            % use the operator defined in the class
            RES = scalar_operator(MultiMatrix(IN1),MultiMatrix(IN2));
            % compare the obtained result to the expected result
            testCase.verifyEqual(double(RES) , expected,'AbsTol',1e-10)
            % check whether we can apply the operator by double arrays on the right
            RES = scalar_operator(MultiMatrix(IN1),IN2);
            % compare the obtained result to the expected result
            testCase.verifyEqual(double(RES) , expected,'AbsTol',1e-10)
            % check whether we can apply the operator by double arrays on the left
            RES = scalar_operator(IN1,MultiMatrix(IN2));
            % compare the obtained result to the expected result
            testCase.verifyEqual(double(RES) , expected,'AbsTol',1e-10)
            % test whether the operator works with a scalar
            RES = scalar_operator(MultiMatrix(IN1),rand());
            RES = scalar_operator(rand(),MultiMatrix(IN1));
        end
        function test_uminus(testCase,N,M,F)
            IN1 = rand(N,M,F)+1i*rand(N,M,F);
            RES = uminus(MultiMatrix(IN1));
            expected = -IN1;
            testCase.verifyEqual(double(RES) , expected,'AbsTol',1e-10)
        end
        function test_trace(testCase,N,F)
            IN = rand(N,N,F)+1i*rand(N,N,F);
            RES = trace(MultiMatrix(IN));
            REStest=zeros(1,1,F);
            for ff=1:F
                REStest(1,1,ff) = trace(IN(:,:,ff));
            end
            testCase.verifyClass(RES,'MultiMatrix')
            testCase.verifyEqual(double(RES),REStest);
        end
        function test_diag(testCase,N,M,F)
            IN = rand(N,M,F)+1i*rand(N,M,F);
            RES = diag(MultiMatrix(IN));
            if (M==1)||(N==1)
                REStest = zeros(max(M,N),max(M,N),F);
            else
                REStest = zeros(min(M,N),1       ,F);
            end
            for ff=1:F
                REStest(:,:,ff) = diag(IN(:,:,ff));
            end
            testCase.verifyClass(RES,'MultiMatrix')
            testCase.verifyEqual(double(RES),REStest);
        end
        %% tests for the size functions
        function test_size(testCase,N,M,F)
            IN = MultiMatrix(rand(N,M,F));
%             testCase.verifyEqual(size(IN) , [N M F]);
            % test the different dimensions
            testCase.verifyEqual(size(IN,1) ,N);
            testCase.verifyEqual(size(IN,2) ,M);
            testCase.verifyEqual(size(IN,3) ,F);
            % test the function with zero outputs
            size(IN);
            % test the function with two outputs
%             [A,B]=size(IN);
%             testCase.verifyEqual(A , N);
%             testCase.verifyEqual(B , M);
            % test the function with three outputs
            [A,B,C] = size(IN);
            testCase.verifyEqual(A , N);
            testCase.verifyEqual(B , M);
            testCase.verifyEqual(C , F);
        end
        function test_length(testCase,N,M,F)
            IN = MultiMatrix(rand(N,M,F));
            testCase.verifyEqual(length(IN) , F);
        end
        %% PLOT function
        function test_plot_MM(testCase,N,M,F,PlotTypes)
            % I just check that the plot function doesn't give any errors
            figure(1321354)
            clf
            IN = MultiMatrix(rand(N,M,F));
            if ~isempty(PlotTypes)
                h = plot(IN,'Config',PlotTypes);
            else
                h = plot(IN);
            end
        end
    end
end
